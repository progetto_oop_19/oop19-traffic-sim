package constraints;

public final class GridConstraints {

    /**
     * 
     */
    public static final int GRID_HEIGHT = 78;

    /**
     * 
     */
    public static final int GRID_WIDTH = 78;

    private GridConstraints() {
    }
}
