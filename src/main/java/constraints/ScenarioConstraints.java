package constraints;

public final class ScenarioConstraints {

    /**
     * 
     */
    public static final int MIN_ROADS = 1;

    /**
     * 
     */
    public static final int MAX_ROADS = 4;

    /**
     * 
     */
    public static final int MIN_TRAFFICLIGHTS = 1;

    /**
     * 
     */
    public static final int MAX_TRAFFICLIGHTS = 4;

    /**
     * 
     */
    public static final int MIN_TOTALCARS = 4;

    /**
     * 
     */
    public static final int MAX_TOTALCARS = 60;

    /**
     * 
     */
    public static final int MIN_LANES_PER_ROAD = 1;

    /**
     * 
     */
    public static final int MAX_LANES_PER_ROAD = 2;

    private ScenarioConstraints() {
    }
}
